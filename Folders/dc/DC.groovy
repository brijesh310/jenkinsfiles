//  https://jenkinsci.github.io/job-dsl-plugin/#path/folder

folder('DC') {
    displayName('DC')
    description('Folder for DC')
}

folder('DC/Build') {
    description('Folder containing all Build Jobs')
}

folder('DC/Build/BuildFromBranch') {
    description('Folder containing all Build Jobs')
}

folder('DC/Deploy') {
    description('Folder containing all Deploy Jobs')
}

folder('DC/Deploy/DEV') {
    description('Folder containing all Deploy DEV Jobs')
}

folder('DC/Deploy/TST') {
    description('Folder containing all Deploy TST Jobs')
}

folder('DC/Deploy/STG') {
    description('Folder containing all Deploy STG Jobs')
}

folder('DC/Deploy/PRD') {
    description('Folder containing all Deploy PRD Jobs')
}


